---
documentclass: extreport
lang: es
fontsize: 14pt
toc: true
title: Técnicas Rudas | Verificación en 2 pasos para Whatsapp
date: Septiembre 25, 2020
geometry: "left=3cm,right=3cm,top=2cm,bottom=1.5cm"
---

# Contexto

En México, las amenazas más grandes para que un atacante acceda a nuestras redes sociales o aplicaciones de mensajería instantánea son la infección de dispositivos con herramientas espía (spyware) o mediante el engaño (phishing), sin embargo, a veces sólo basta con aprovecharse de cómo funcionan los sistemas actuales para que haya accesos no autorizados a nuestras cuentas/aplicaciones. En el caso de las aplicaciones de mensajería instantánea como Whatsapp es posible acceder a cualquier cuenta ajena aprovechándose de cómo funciona actualmente la asignación de números/líneas telefónicas.

Por ejemplo, si una línea expira y se nos es reasignada[^vice] por accidente, si intencionalmente engañamos a una compañía o sobornamos/amenzamos a una persona dentro de la compañía para que nos dé una tarjeta SIM con el mismo número que otra persona usa en Whatsapp ahora podríamos leer todas sus conversaciones actuales (no las pasadas) sin que la persona lo sepa. 

[^vice]: https://www.vice.com/en_us/article/bv8mqd/how-i-hacked-whatsapp-account

Para mitigar lo anterior se recomienda activar la verificación en 2 pasos (protección de nuestro número) y las notificaciones de seguridad (detección de clonado de otros números).

# Whatsapp

## Verificación en 2 pasos paso a paso

1. Tocar los 3 puntos en la parte superior derecha y seleccionar Ajustes --> Cuenta --> Verificación en 2 pasos.

![Pantalla principal de Whatsapp](images/verificacion-2-pasos-whatsapp-01.png)

![Pantalla de Ajustes de Whatsapp](images/verificacion-2-pasos-whatsapp-02.png)

![Pantalla de Cuenta de Whatsapp](images/verificacion-2-pasos-whatsapp-03.png)

![Pantalla de Verificación en 2 pasos](images/verificacion-2-pasos-whatsapp-04.png)

2. Introducir un PIN de 6 dígitos, volverlo a introducir para confirmar y opcionalmente introducir un correo electrónico para poder restablecer el PIN en caso de olvido. Si olvidamos el PIN y no ponemos un correo electrónico válido no podremos volver a usar Whatsapp con ese número por lo que es recomendable hacerlo.

![Pantalla de creación de PIN de 6 dígitos](images/verificacion-2-pasos-whatsapp-05.png)

![Pantalla de confirmación del PIN](images/verificacion-2-pasos-whatsapp-06.png)

![Pantalla para añadir correo electrónico en caso de olvido (opcional)](images/verificacion-2-pasos-whatsapp-07.png)

![Pantalla con la advertencia sobre no poder volver a usar Whatsapp si olvidamos el PIN y no introduciome un correo electrónico válido](images/verificacion-2-pasos-whatsapp-07.png)

3. La verificación en 2 pasos para Whatsapp ya está activada. Ahora en cada nueva instalación de Whatsapp necesitarás introducir el PIN, haciendo inefectivo el clonado de tarjetas SIM (al menos para Whatsapp).

![Pantalla de verificación en 2 pasos activa. Incluye la opción e desactivarla, cambiar el PIN o añadir correo electrónico si aún no se ha hecho](images/verificacion-2-pasos-whatsapp-09.png)

4. Por último, activa el mostrar notificaciones de seguridad para saber cuando Whatsapp detecte que alguien en nuestra lista ha reinstalado Whatsapp o cambiado de dispositivo y así poder preguntarles si en realidad lo hizo dicha persona. Para hacer esto vuelva a ir a Ajustes --> Cuenta --> Seguridad. 

![Pantalla de seguridad](images/verificacion-2-pasos-whatsapp-10.png)

## Consideraciones de la verificación en 2 pasos.

- Nunca compartas el PIN con nadie.

- Whatsapp tiene la mala práctica[^faq] de no verificar el correo eléctronico que introducimos. Queda en nuestra responsibilidad verificar que el correo eléctrínico que ponemos es el correcto.

[^faq]: https://faq.whatsapp.com/general/verification/using-two-step-verification/?lang=en

- Una vez activada la verificación en 2 pasos Whatsapp regularmente nos pedirá introducir el PIN para qu eno se nos olvide. La única forma de descativar el recordatorio es desactivando la verificación.

## Bibliografía

