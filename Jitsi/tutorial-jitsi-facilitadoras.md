---
documentclass: extreport
lang: es
fontsize: 14pt
title: Técnicas Rudas | Introducción a Jitsi para Facilitadoras
date: Diciembre 14, 2020
geometry: "left=3cm,right=3cm,top=2cm,bottom=1.5cm"
---
\newpage
# Índice
1. [¿Qué es Jitsi?](#intro)
2. [Practicas Buenas](#practicas)
3. [Jitsi desde un navegador](#navegador)
    * [Iniciar sesión y configurar idioma](#iniciar)
    * [Crear y/o configurar salas](#crear)
    * [Fuentes de Audio y Video](#fuentes)
    * [Invitar a otras participantes](#invitar)
    * [Proteger la sala con contraseña fuerte](#contraseña)
    * [Chat](#chat)
    * [Compartir la pantalla](#compartir)
    * [Hacer presentadoras a otras personas](#presentadoras)
    * [Grabar sesiones](#grabar)
    * [Ajustar calidad de la llamada](#ajustar)
4. [Jitsi desde la aplicación móvil](#movil)
    * [Instalando la Aplicación Jitsi Meet](#instalar)
    * [Usar el Servidor de Greenhost](#servidor)
    * [Permisos](#permisosmovil)
    * [Ajustes](#ajustesmovil)
    * [Crear una sala](#crearsala)
    * [Cambiar Nombre/Apodo](#cambiarnombre)
    * [Audio y Video](#audiovideo)
    * [Proteger la sala con contraseña](#contraseñamovil)
    * [Calidad de la llamada](#calidadmovil)
    * [Abrir el Chat](#chatmovil)
5. [Instancias de Jitsi](#instancias)

\newpage

# ¿Qué es Jitsi? <a name="intro"></a>

Jitsi (o Jitsi Meet) es un plataforma para llamadas grupales de código abierto y software libre. 

Es una plataforma para llamadas grupales o presentaciones, con un enfoque en la privacidad. Jitsi funciona desde un navegador o la aplicación móvil. No es necesario tener una cuenta de Jitsi para crear una sala de conferencia, sólo abres una sala usando un enlace en Chrome, Firefox o desde la aplicación móvil y puedes compartir este enlace con otras participantes. Jitsi puede ser alojado en cualquier servidor y hay varias instancias operadas por organizaciones éticas, enfocadas en la privacidad y seguridad digital de los usuarios. La instancia de Jitsi que mostramos en este tutorial es operada por Greenhost, lo cual es un ejemplo de estos servicios éticos. 

\newpage

# Practicas Buenas <a name="practicas"></a>
* Lo que te compartimos más que ser buenas prácticas son prácticas resultado de probarlo entre personas con diversas condiciones de tecnología (p. ejemplo diferentes sistema operativos, diferentes capacidades de hardware) y con acceso diferenciado a Internet

* **Chrome o Firefox** son los mejores navegadores para Jitsi, y el servicio a veces funciona mejor si todos los usuarios están usando la misma plataforma - por ejemplo, si todos están usando la aplicación móvil o todos están usando el navegador Chrome al mismo tiempo. Si tienen problemas con conectividad una solución puede ser eligir la misma forma de acceder a Jitsi.

* **Recuerda cambiar el servidor en los ajustes de la aplicación móvil**

* Si tu Internet no es muy estable o tienes poco ancho de banda apaga tu vídeo

* Para facilitar escucharse puedes apagar tu micrófono y solo activarlo cuando necesitas hablar

* La reunión mejora si puedes usar audífonos 

* Si dejas de escuchar recarga la página y probablemente el problema se soluciona

\newpage

# Jitsi desde un Navegador <a name="navegador"></a>

## Iniciar sesión y configurar idioma <a name="iniciar"></a>

En este tutorial usaremos de ejemplo a una persona llamada Josefina la cual quiere facilitar una presentación usando Jitsi.  

El primer paso es ir a la dirección web de la instancia de Jitsi e iniciar sesión:

[https://meet.greenhost.net/](https://meet.greenhost.net/)

### Pagina de inicia
Jitsi genera un enlace único usando cuatro palabras diferentes. Puedes eligir un enlace diferente pero el beneficio del enlace generado en automático es que es único y no vas a entrar a una sala existente con otras personas. Luego mostramos como proteger la sala de reunión con una contraseña. 

![](images/jitsi-01.png)

Si la pantalla de inicia no te apareciera en español no te preocupes pues se puede modificar. Da clic en en símbolo de las configuraciones en la parte superior derecha y selecciona "más." Luego selecciona el idioma a usar y da clic en el botón "Aceptar" para guardar los cambios. 

![](images/jitsi-02.png)

### Permisos
Dependiendo del navegador que usas te puede preguntar si autorizas el uso de tu cámara y micrófono. 

![](images/jitsi-permisos.png)

\newpage

## Crear y/o configurar salas <a name="crear"></a>

Cuando quieres crear una sala, da clic en el botón "Ir" al lado derecho. 

![](images/jitsi-01.png)

### La sala de reunión:

![](images/jitsi-03.png)

Puedes ajustar el micrófono y la cámara con los siguientes botones. Los dos están apagados en la captura abajo:

![](images/jitsi-004.png)

Para cambiar las configuraciones de la sala puedes dar clic en los tres botones chicos y luego escoger "Configuración."

![](images/jitsi-last1.png)

\newpage

## Fuentes de Audio y Video <a name="fuentes"></a>
Para cambiar las configuraciones de la sala puedes dar clic en los tres botones chicos y luego escoger "Configuración."

![](images/jitsi-last1.png)

En Configuración puedes cambiar los fuentes de audio y video que prefieres (audífonos, micrófono interno o externo, cámara interna o externa) 

![](images/jitsi-last2.png)

En "Más" puedes cambiar los ajustes para empezar la llamada con los micrófonos y videos apagados. Esto ayuda si personas tienen mucho ruido de fondo cuando suben a la sala. 

![](images/jitsi-last3.png)

\newpage

## Invitar a otras participantes <a name="invitar"></a>

Para invitar a otras participantes, puedes seleccionar el botón de "Invitar a más personas" en la sala principal. Luego puedes copiar el enlace o compartir una invitación a la sala.

![](images/jitsi-04.png)

\newpage

## Proteger la sala con contraseña fuerte <a name="contraseña"></a>

Sin una contraseña fuerte, cualquier persona que tiene acceso al enlace de la sala puede entrar y crear caos. Por eso activamos una contraseña fuerte:

### Una contraseña fuerte tiene **más que 8 símbolos, letras, y números y evita el uso de palabras obvias o personales como nombres y fechas de nacimiento.** Evita el uso de una contraseña  fácilmente adivinada como "1234321" o "contraseña123" y evita la repetición de contraseñas para otras cosas. La mejor contraseña es una combinación inesperada y única.

Para activar a una contraseña para la sala de Jitsi, escoge el botón de "opciones de seguridad" en la parte inferior derecha de la pantalla. 

![](images/jitsi-07.png)

Luego escoge "Agregar la clave" y crea una contraseña con letras y números.

![](images/jitsi-08.png)

Manda la contraseña a las participantes de una manera confiable. Una estrategia de seguridad si la llamada es muy secreto, es mandar el enlace de la sala por correo y mandar la contraseña por una aplicación de mensajes cifrados, como Signal. 

![](images/jitsi-09.png)

Aquí usamos un ejemplo de una contraseña fuerte para Jitsi: *"dq@wfk!sw"*

\newpage

## Chat <a name="chat"></a>

Para abrir el chat da clic en la parte inferior izquierda donde dice "Chat público."

![](images/jitsi-10.png)

Te pide un apodo para mostrar a las otras participantes. 

![](images/jitsi-11.png)

Puedes minimizar la ventana del chat durante la llamada o abrirlo otra vez usando el mismo botón.

![](images/jitsi-12.png)

\newpage

## Compartir la Pantalla <a name="compartir"></a>
Escoge este botón en la parte inferior de la página. 

![](images/jitsi-13.png)

Las opciones en orden son "compartir tu pantalla completa" para mostrar cualquier cosa en tu computadora, "compartir solo la ventana de una aplicación," o "compartir una ventana del navegador." 

![](images/jitsi-14.png)

**Sugerencia**: Si en algún momento las otras personas en la sala no ven lo que estás compartiendo o se les quedó "congelada" la imagen te recomendamos recargar la página.

\newpage

## Hacer presentadoras a otras personas <a name="presentadoras"></a>

Dando clic en los tres botones chicos de cualquier participante la persona que creó la sala puede habilitar la opción de "Dar Privilegios de Moderador", es decir, de que puede compartir su pantalla y cambiar configuraciones de la sala de reunión también. Posteriormente no se puede retomar dicho rol. 

![](images/jitsi-05.png)

![](images/jitsi-06.png)

Ahora cualquier persona que tiene permisos de moderador puede compartir su pantalla

\newpage


## Grabar sesiones <a name="grabar"></a>

Al momento no puedes grabar una sesión con la instancia de Jitsi en Greenhost. Necesitas una instancia de Jitsi en un servidor donde se puede descargar una grabación de video. 

Si estas usando una instancia de Jitsi que te permite grabar la sesión, lo haces dando clic en las configuraciones al fondo derecho. Luego escoges "Iniciar grabación"

![](images/jitsi-15.png)

![](images/jitsi-15-1.png)

\newpage

## Ajustar calidad de la llamada <a name="ajustar"></a>

Hay varias maneras de ajustar la llamada para tener mejorar la conectividad y calidad. 

Puedes abrir las configuraciones en la parte inferior derecha y escoger "ajustar la calidad video."

![](images/jitsi-15.png)

Si hay problemas de conectividad trata de usar un nivel de menos calidad. 

![](images/jitsi-16.png)

También puedes ver métricas de la calidad de tu audio y video con el botón al lado de los símbolos de micrófono y cámara:

![](images/jitsi-17.png)

Para ver la calidad de otras participantes y la latencia de sus conexiones puedes dar clic en la esquina en la ventana de cualquier persona. 

![](images/jitsi-18.png)

**Nota** Si la conectividad no mejora con las sugerencias arriba, considera apagando el video de todas/os que no necesiten tenerla aprendida. Pueden dejar la presentadora con su pantalla compartida o su video aprendida y tomar turnos con el video para los demás. 

\newpage

# Usando Jitsi en la Aplicación Móvil <a name="movil"></a>

## Instalando la Aplicación Jitsi Meet <a name="instalar"></a>

La aplicación para móvil se llama Jitsi Meet y fue desarrollado por la empresa 8x8 Inc.

![](images/jitsi-movil-01.png){ width=60% }

\newpage

## Usar el Servidor de Greenhost <a name="servidor"></a>
**Importante!**
Para usar los servicios éticos y seguros de Greenhost para la instancia de Jitsi, tenemos que cambiar el servidor por defecto que la aplicación va a usar para alojar la llamada. La aplicación viene con el servidor https://meet.jit.si por defecto de la empresa 8x8 pero podemos cambiarlo a Greenhost.

![](images/jitsi-movil-08.png){ width=60% }

Para cambiar el servidor escriba "https://meet.greenhost.net" en este lugar.

![](images/jitsi-movil-08.png){ width=60% }

Ahora el enlace generado automáticamente cuando creas una sala de reunión es el [URL del servidor] + [nombre de la sala], por ejemplo: "https://meet.greenhost.net/UnoDosTresCuatro." 

\newpage

## Permisos <a name="permisosmovil"></a>
Jitsi te va preguntar si quieres dar acceso al micrófono y la cámara. Si solo vas a hacer llamadas de audio puedes decir "No permitir" para permisos de la cámara. 

![](images/jitsi-movil-02.png){ width=60% }

Jitsi te va preguntar si quieres dar acceso a tus datos de calendario. **No es requerido** para la funcionalidad de la aplicación, solo si quieres sincronizar fechas de reuniones.

![](images/jitsi-movil-03.png){ width=60% }

\newpage

## Ajustes <a name="ajustesmovil"></a>
Para cambiar ajustes primero, selecciona las lineas hacia la izquierda.

![](images/jitsi-movil-05.png){ width=60% }

Luego selecciona “Ajustes.”

![](images/jitsi-movil-04.png){ width=60% }

\newpage

## Crear una sala <a name="crearsala"></a>
El la ventana principal de la aplicación, Jitsi genera enlaces automáticamente. Puedes dar clic en el nombre generado y cambiarlo o usar lo que Jitsi generó. Después da clic en "Crear/Unirse" y la sala empieza.
![](images/jitsi-movil-19.png){ width=60% }

\newpage

## Cambiar Nombre/Apodo <a name="cambiarnombre"></a>
En ajustes puedes entrar un apodo opcional. Si quieres cambiar esto, hazlo antes de empezar una llamada, si no vas a tener que salir de la llamada, cambiarlo, y regresar. 

![](images/jitsi-movil-06.png){ width=60% }

\newpage 

## Audio y Video <a name="audiovideo"></a>
En ajustes puedes seleccionar si tu audio y video empiezan apagados o aprendidos. Solo deslice el botón para cambiarlo.

![](images/jitsi-movil-07.png){ width=60% }

### Apagar la cámara antes de la llamada
Cuando no estás en una sala de reunión, desliza el botón a la derecha si solo quieres habilitar voz o regresa el botón a la izquierda si quieres mostrar video. 
![](images/jitsi-movil-14.png){ width=60% }

### Apagar el audio/la cámara durante de la llamada
Durante una llamada, usa los botones al fondo de la aplicación para aprender o apagar el audio y la cámara.
![](images/jitsi-movil-15.png){ width=60% }

\newpage

## Proteger la sala con contraseña <a name="contraseñamovil"></a>

Para activar a una contraseña para la sala de Jitsi, abra los ajustes con clic en los puntos chicos.

![](images/jitsi-movil-10.png){ width=60% }

Luego escoge la opción de "Agregar reunión Contraseña." 

![](images/jitsi-movil-11.png){ width=60% }

Manda la contraseña a las participantes de una manera confiable. Una estrategia de seguridad si la llamada es muy secreto, es mandar el enlace de la sala por correo y mandar la contraseña por una aplicación para mensajes cifrados, como Signal.

![](images/jitsi-movil-12.png){ width=60% }

\newpage

## Calidad de la llamada <a name="calidadmovil"></a>
En las configuraciones puedes cambiar "Habilitar el modo de ancho de banda bajo" si la llamada tiene problemas con conectividad.

![](images/jitsi-movil-13.png){ width=60% }

## Abrir el Chat <a name="chatmovil"></a>
Da clic en el botón izquierda para abrir la ventana del chat. 

![](images/jitsi-movil-16.png){ width=60% }

Si no tienes un apodo, te va pedir algo para mostrar a las otras participantes. Puedes usar tu nombre o algo diferente. 

![](images/jitsi-movil-17.png){ width=60% }

Y para salir del chat escoge el botón para regresar en la parte superior izquierda.

![](images/jitsi-movil-18.png){ width=60% }

\newpage

# Instancias de Jitsi <a name="instancias"></a>
La instancia[^1] que usamos en este tutorial es [https://meet.greenhost.net](https://meet.greenhost.net)

#### Otras Instancias de Jitsi:

* [https://meet.jit.si](https://meet.jit.si)
* [https://meet.mayfirst.org](https://meet.mayfirst.org)
* [https://calls.disroot.org/](https://calls.disroot.org/)
* [https://vc.autistici.org/](https://vc.autistici.org/)

[^1]: Instancia: Es una instalación que usa un servicio/tecnología pero que no es toda tecnología en sí. Por ejemplo: Empresas como Gmail, Hotmail y Yahoo o colectivos como Riseup o Disroot operan sus propias instancias de correo electrónico pero individualmente ninguna empresa o colectivo es el correo electrónico en sí mismo. La misma lógicas se puede aplicar a otro tipo tecnologías.
\newpage

