---
documentclass: extreport
lang: es
fontsize: 14pt
title: Técnicas Rudas | Introducción a Jitsi para Participantes
date: Diciembre 21, 2020
geometry: "left=3cm,right=3cm,top=2cm,bottom=1.5cm"
---

# Índice

### 1. ¿Qué es Jitsi?
### 2. Practicas Buenas
### 3. Jitsi desde un Navegador
### 4. Jitsi desde la Aplicación Móvil

\newpage

# ¿Qué es Jitsi? 

Jitsi (o Jitsi Meet) es un plataforma para llamadas grupales o presentaciones, con un enfoque en la privacidad, y es de código abierto y software libre. 

Jitsi funciona desde un navegador o la aplicación móvil. No es necesario tener una cuenta de Jitsi para crear una sala de conferencia, sólo abres una sala usando un enlace en Chrome, Firefox o desde la aplicación móvil y puedes compartir este enlace con otras participantes. 
\newpage

# Practicas Buenas 

* **Chrome o Firefox** son los mejores navegadores para Jitsi, y el servicio a veces funciona mejor si todos los usuarios están usando la misma plataforma - por ejemplo, si todos están usando la aplicación móvil o todos están usando el navegador Chrome al mismo tiempo. Si tienen problemas con conectividad una solución puede ser eligir la misma forma de acceder a Jitsi.

* **Recuerda cambiar el servidor en los ajustes de la aplicación móvil**

* Si tu Internet no es muy estable o tienes poco ancho de banda apaga tu vídeo

* Para facilitar escucharse puedes apagar tu micrófono y solo activarlo cuando necesitas hablar

* La reunión mejora si puedes usar audífonos 

* Si dejas de escuchar recarga la página y probablemente el problema se soluciona

\newpage

# Jitsi desde un Navegador 

## Unirse a la sala de reunion <a name="unirse"></a>

### Pagina de inicia
Para entrar a la sala de reunión sigue el enlace tu navegador. Si no tienes Chrome y Firefox, puedes descargar alguno desde el navegador que ya tienes o usar la aplicación móvil en tu celular (instrucciones abajo). 
![](images/jitsi-03.png)

Si la sala tiene contraseña, te lo pedirá ahora.

### Permisos
Dependiendo del navegador que usas te puede preguntar si autorizas el uso de tu cámara y micrófono. 

![](images/jitsi-permisos.png)

## Cambiar Idioma 
Para cambiar las configuraciones de la sala puedes dar clic en los tres botones chicos en la parte inferior derecha y luego escoger "Configuración."

![](images/jitsi-last1.png)

Selecciona "más." Luego selecciona el idioma a usar y da clic en el botón "Aceptar" para guardar los cambios. 

![](images/idioma.png)

\newpage

## Fuentes de Audio y Video 
Para cambiar las configuraciones del audio y video puedes dar clic en los tres botones chicos y luego escoger "Configuración."

![](images/jitsi-last1.png)

En Configuración puedes cambiar los fuentes de audio y video que prefieres (audífonos, micrófono interno o externo, cámara interna o externa) 

![](images/jitsi-last2.png)

\newpage


## Chat 

Para abrir el chat da clic en la parte inferior izquierda donde dice "Chat público."

![](images/jitsi-10.png)

Te pide un apodo para mostrar a las otras participantes. Se puede cambiar más tarde desde las "configuraciones" y luego "perfil." 

![](images/jitsi-11.png)

Puedes minimizar la ventana del chat durante la llamada o abrirlo otra vez usando el mismo botón.

![](images/jitsi-12.png)

\newpage

## Compartir la Pantalla 
Para compartir tu pantalla, necesitas permisos de moderador que te tiene que dar la facilitadora de la sala. 

Cuando ya tienes permisos, escoge este botón en la parte inferior de la página. 

![](images/jitsi-13.png)

Las opciones en orden son "compartir tu pantalla completa" para mostrar cualquier cosa en tu computadora, "compartir solo la ventana de una aplicación," o "compartir una ventana del navegador." 

![](images/jitsi-14.png)

**Sugerencia**: Si en algún momento las otras personas en la sala no ven lo que estás compartiendo o se les quedó "congelada" la imagen te recomendamos recargar la página.

\newpage


# Jitsi desde la Aplicación Móvil 

## Instalando la Aplicación Jitsi Meet 

La aplicación para móvil se llama Jitsi Meet y fue desarrollado por la empresa 8x8 Inc.

![](images/jitsi-movil-01.png){ width=60% }

\newpage

## Usar el Servidor de Greenhost 
**Importante!**
Para usar los servicios éticos y seguros de Greenhost para la instancia de Jitsi, tenemos que cambiar el servidor por defecto que la aplicación va a usar para alojar la llamada. La aplicación viene con el servidor https://meet.jit.si por defecto de la empresa 8x8 pero podemos cambiarlo a Greenhost.

![](images/jitsi-movil-08.png){ width=60% }

Para cambiar el servidor escriba "https://meet.greenhost.net" en este lugar.

![](images/jitsi-movil-08.png){ width=60% }

Ahora el enlace generado automáticamente cuando creas una sala de reunión es el [URL del servidor] + [nombre de la sala], por ejemplo: "https://meet.greenhost.net/UnoDosTresCuatro." 

\newpage

## Permisos 
Jitsi te va preguntar si quieres dar acceso al micrófono y la cámara. Si solo vas a hacer llamadas de audio puedes decir "No permitir" para permisos de la cámara. 

![](images/jitsi-movil-02.png){ width=60% }

Jitsi te va preguntar si quieres dar acceso a tus datos de calendario. **No es requerido** para la funcionalidad de la aplicación, solo si quieres sincronizar fechas de reuniones.

![](images/jitsi-movil-03.png){ width=60% }

\newpage

## Ajustes 
Para cambiar ajustes primero, selecciona las lineas hacia la izquierda.

![](images/jitsi-movil-05.png){ width=60% }

Luego selecciona “Ajustes.”

![](images/jitsi-movil-04.png){ width=60% }

\newpage

## Unirse a una sala 
Si escoges el enlace que recibiste para la sala, puedes copiar y pegarlo en la pagina de inicia de la aplicación. Luego da clic en "Crear/Unirse" y puedes entrar. Después entra la contraseña si la sala la requiere. 

![](images/jitsi-movil-19.png){ width=60% }

\newpage

## Cambiar Nombre/Apodo 
En ajustes puedes entrar un apodo opcional. Si quieres cambiar esto, hazlo antes de empezar una llamada, si no vas a tener que salir de la llamada, cambiarlo, y regresar. 

![](images/jitsi-movil-06.png){ width=60% }

\newpage 

## Audio y Video
En ajustes puedes seleccionar si tu audio y video empiezan apagados o aprendidos. Solo deslice el botón para cambiarlo.

![](images/jitsi-movil-07.png){ width=60% }

### Apagar la cámara antes de la llamada
Cuando no estás en una sala de reunión, desliza el botón a la derecha si solo quieres habilitar voz o regresa el botón a la izquierda si quieres mostrar video. 

![](images/jitsi-movil-14.png){ width=60% }

### Apagar el audio/la cámara durante de la llamada
Durante una llamada, usa los botones al fondo de la aplicación para aprender o apagar el audio y la cámara.

![](images/jitsi-movil-15.png){ width=60% }

\newpage

## Chat
Da clic en el botón izquierda para abrir la ventana del chat. 

![](images/jitsi-movil-16.png){ width=60% }

Si no tienes un apodo, te va pedir algo para mostrar a las otras participantes. Puedes usar tu nombre o algo diferente. 

![](images/jitsi-movil-17.png){ width=60% }

Y para salir del chat escoge el botón para regresar en la parte superior izquierda.

![](images/jitsi-movil-18.png){ width=60% }

\newpage



